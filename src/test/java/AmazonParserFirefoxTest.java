import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import pages.AmazonBookPage;
import pages.AmazonSearchPage;

public class AmazonParserFirefoxTest extends AmazonParserBySearch {
    @BeforeTest
    @Override
    public void setupDriver() {
        System.setProperty("webdriver.gecko.driver", "C:/Program Files/WebDrivers/geckodriver.exe");
        setDriver(new FirefoxDriver());
        setSearchPage(new AmazonSearchPage(getDriver(), getSiteUrl()));
        setBookPage(new AmazonBookPage(getDriver(), getBookUrl()));
    }

    @AfterTest
    @Override
    public void closeDriver() {
        getDriver().quit();
    }
}
