import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import pages.AmazonBookPage;
import pages.AmazonSearchPage;
import pages.BookContainer;

import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

public abstract class AmazonParserBySearch {
    private WebDriver driver;
    private String siteUrl = "https://www.amazon.com";
    private String bookUrl = "https://www.amazon.com/Head-First-Java-Kathy-Sierra/dp/0596009208/";

    private AmazonSearchPage searchPage;
    private AmazonBookPage bookPage;

    @BeforeTest
    public abstract void setupDriver();

    public String getBookTitleByPage(String bookUrl) {
        driver.get(bookUrl);
        String name = driver.findElement(By.xpath("//span[@id=\"productTitle\"]")).getText();
        return name;
    }

    public ResultBookInfo filterSearchResult(BookContainer book) {
        ResultBookInfo result =  new ResultBookInfo(
                book.getTitle(),
                book.getAuthor(),
                book.getPriceFromBookElement(),
                book.getRatingFromBookElement(),
                book.getIsBestsellerFromBookElement()
        );
        System.out.println(result.toString());
        return result;
    }

    @Test
    public void filterSearchResults() {
        // Find books from main page and parse data
        searchPage.executeSearchQuery("Java");
        List<BookContainer> searchResults = searchPage.getSearchResults();
        List<ResultBookInfo> filteredSearchResults = searchResults.stream().map(r -> filterSearchResult(r))
                .collect(Collectors.toList());

        // Left only book titles
        List<String> searchResultsTitles = filteredSearchResults.stream().map(ResultBookInfo::getName)
                .collect(Collectors.toList());

        // Parse data from book page, parse data and check, if this book in search results
        String bookTitle = getBookTitleByPage(bookUrl);

        Assert.assertTrue(searchResultsTitles.contains(bookTitle));
    }

    @AfterTest
    public abstract void closeDriver();

    public WebDriver getDriver() {
        return driver;
    }

    public void setDriver(WebDriver driver) {
        this.driver = driver;
    }

    public String getSiteUrl() {
        return siteUrl;
    }

    public void setSiteUrl(String siteUrl) {
        this.siteUrl = siteUrl;
    }

    public String getBookUrl() {
        return bookUrl;
    }

    public void setBookUrl(String bookUrl) {
        this.bookUrl = bookUrl;
    }

    public AmazonSearchPage getSearchPage() {
        return searchPage;
    }

    public void setSearchPage(AmazonSearchPage searchPage) {
        this.searchPage = searchPage;
    }

    public AmazonBookPage getBookPage() {
        return bookPage;
    }

    public void setBookPage(AmazonBookPage bookPage) {
        this.bookPage = bookPage;
    }
}
