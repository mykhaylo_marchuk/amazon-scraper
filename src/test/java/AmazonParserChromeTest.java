import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import pages.AmazonBookPage;
import pages.AmazonSearchPage;

public class AmazonParserChromeTest extends AmazonParserBySearch {
    @BeforeTest
    @Override
    public void setupDriver() {
        System.setProperty("webdriver.chrome.driver", "C:/Program Files/WebDrivers/chromedriver.exe");
        setDriver(new ChromeDriver());
        setSearchPage(new AmazonSearchPage(getDriver(), getSiteUrl()));
        setBookPage(new AmazonBookPage(getDriver(), getBookUrl()));
    }

    @AfterTest
    @Override
    public void closeDriver() {
        getDriver().quit();
    }
}
