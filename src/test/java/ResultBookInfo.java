public class ResultBookInfo {
    private String name;
    private String author;
    private double price;
    private double rating;
    private boolean isBestseller;

    public ResultBookInfo(String name, String author, double price, double rating, boolean isBestseller) {
        this.name = name;
        this.author = author;
        this.price = price;
        this.rating = rating;
        this.isBestseller = isBestseller;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public double getRating() {
        return rating;
    }

    public void setRating(double rating) {
        this.rating = rating;
    }

    public boolean isBestseller() {
        return isBestseller;
    }

    public void setBestseller(boolean bestseller) {
        isBestseller = bestseller;
    }

    @Override
    public String toString() {
        return String.format("Title: %s\nAuthor: %s\nPrice: %s\nRating: %s\n%s",
                name,
                author,
                String.valueOf(price),
                String.valueOf(rating),
                isBestseller ? "The bestseller\n" : "\n");
    }
}
