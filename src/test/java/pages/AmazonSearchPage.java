package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

public class AmazonSearchPage {
    private WebDriver driver;

    @FindBy(xpath = "//input[@id='twotabsearchtextbox']")
    private WebElement searchField;

    @FindBy(xpath = "//div[contains(@class, 's-search-results')]")
    private WebElement searchResultsContainer;

    @FindBy(xpath = "//div[contains(@class, 's-result-list-placeholder')]")
    private WebElement searchResultBreakLine;

    public AmazonSearchPage(WebDriver driver, String url) {
        driver.get(url);
        PageFactory.initElements(driver, this);
        this.driver = driver;
    }

    public void executeSearchQuery(String query) {
        searchField.sendKeys(query);
        searchField.submit();
    }

    public List<BookContainer> getSearchResults() {
        // Waiting for search results loading
        while (true) {
            try {
                driver.manage().timeouts().pageLoadTimeout(10, TimeUnit.SECONDS);
                if(searchResultBreakLine.findElements(By.xpath("./*")).size() != 0) {
                    return searchResultsContainer.findElements(By.xpath("./*"))
                            .stream().map(BookContainer::new).collect(Collectors.toList());
                }
            }
            catch (NoSuchElementException e) {
                driver.manage().timeouts().pageLoadTimeout(10, TimeUnit.SECONDS);
            }
        }
    }
}
