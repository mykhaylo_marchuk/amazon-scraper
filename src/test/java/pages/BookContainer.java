package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;

public class BookContainer {
    private WebElement container;

    private WebElement title;
    private WebElement author;
    private WebElement price;
    private WebElement rating;
    private WebElement isBestseller;

    public BookContainer(WebElement container) {
        this.container = container;
        this.title = getTitleWebElement();
        this.author = getAuthorWebElement();
        this.price = getPriceWebElement();
        this.rating = getRatingWebElement();
        this.isBestseller = getBestsellerElement();
    }

    private WebElement getTitleWebElement() {
        return container.findElement(By.tagName("h2"));
    }

    private WebElement getAuthorWebElement() {
        return container.findElement(By.xpath(".//a[contains(@class, \"a-size-base a-link-normal\")]"));
    }

    private WebElement getPriceWebElement() {
        try {
            return container.findElement(
                    By.xpath(".//span[@class=\"a-offscreen\"]")
            );
        }
        catch (NoSuchElementException e) {
            try {
                return container.findElement(
                        By.xpath(".//span[contains(@class, \"a-color-base\") and contains(text(), \"$\")]")
                );
            }
            catch (NoSuchElementException ee) {
                return null;
            }
        }
    }

    private WebElement getRatingWebElement() {
        try {
            return container.findElement(By.xpath(".//span[contains(@aria-label, 'out of 5 stars')]"));
        }
        catch (NoSuchElementException e) {
            return null;
        }
    }

    private WebElement getBestsellerElement() {
        try {
            return container.findElement(By.xpath(".//span[@data-a-badge-color=\"sx-orange\"]"));
        }
        catch (NoSuchElementException e) {
            return null;
        }
    }

    public String getTitle() {
        return title.getText();
    }

    public String getAuthor() {
        return author.getText();
    }

    public double getPriceFromBookElement() {
        try {
            return Double.parseDouble(price.getText().substring(1));
        }
        catch (Exception e) {
            return 0;
        }
    }

    public double getRatingFromBookElement() {
        try {
            return Double.parseDouble(rating.getText().substring(0, 2));
        }
        catch (Exception e) {
            return 0;
        }
    }

    public boolean getIsBestsellerFromBookElement() {
        return isBestseller != null;
    }
}
