package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AmazonBookPage {
    private WebDriver driver;

    @FindBy(xpath = "//span[@id=\"productTitle\"]")
    private WebElement titleWebElement;

    public AmazonBookPage(WebDriver driver, String url) {
        driver.get(url);
        PageFactory.initElements(driver, this);
        this.driver = driver;
    }

    public String getTitle() {
        return titleWebElement.getText();
    }
}
